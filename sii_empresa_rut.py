import pycurl
from urllib import urlencode
from StringIO import StringIO as BytesIO
from openpyxl import load_workbook
from openpyxl.utils import rows_from_range

# Globals

input_file_name = 'docs/rut_diciembre_2016.xlsx'
output_file_name = 'result/rut_diciembre_2016_test.xlsx'
rut_range = 'B2:B5'

# Fixed Globals
rut_data = []
sii_options = [
            'FACTURA ELECTRONICA',
            'FACTURA NO AFECTA O EXENTA ELECTRONICA',
            'BOLETA ELECTRONICA',
            'BOLETA EXENTA ELECTRONICA',
            'GUIA DESPACHO ELECTRONICA',
            'NOTA DEBITO ELECTRONICA',
            'NOTA CREDITO ELECTRONICA',
            'LIBROS CONTABLES ELECTRONICOS']

doc_cols = ['C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

class SIIRequest(object):
    def __init__(self):
        self.url = 'https://palena.sii.cl/cvc_cgi/dte/ee_empresa_rut'

    def get_rut_info(self, rut, dv):
        c = pycurl.Curl()
        c.setopt(c.URL, self.url)

        buffer = BytesIO()
        c.setopt(c.WRITEDATA, buffer)

        post_data = {'RUT_EMP': rut, 'DV_EMP': dv}
        c.setopt(c.POSTFIELDS, urlencode(post_data))
        c.perform()

        print('Rut: %s-%s' % (rut, dv))
        print('Response Code: %d ' % c.getinfo(c.RESPONSE_CODE))
        print('Total Time: %f ms' % c.getinfo(c.TOTAL_TIME))
        print('---\n')

        html_response = buffer.getvalue()

        buffer.close()
        c.close()

        return html_response

class XLSXParser(object):
    def __init__(self):
        self.fl = input_file_name # Input File Name.
        self.rut_col = 'B'
        self.rut_row = 2

        self.wb = load_workbook(self.fl)
        self.ws = self.wb.active

    def read_rut_col(self):
        # Rut Range
        for r in self.ws.iter_rows(range_string = rut_range):
            cell = r[0]
            rut, dv = cell.value.split('-')
            rut_data.append({'cell': cell.coordinate, 'row': cell.row, 'rut': rut, 'dv': dv})
        return rut_data

    def write_col(self, pos, val):
        self.ws[pos] = val

    def save(self, fn):
        self.wb.save(filename = fn)

def main():
    print "*** Iniciando Revision de RUT SII ***"
    # Get Rut
    xls = XLSXParser()
    print ">>> Archivo de Entrada: %s" % xls.fl

    xls.read_rut_col()

    # Get Info Sii by Rut
    for rd in rut_data:
        req = SIIRequest()
        html = req.get_rut_info(rd['rut'], rd['dv'])
        rd['docs'] = []
        for idx, doc in enumerate(sii_options):
            if doc in html:
                pos = "%s%i" % (doc_cols[idx], rd['row'])
                rd['docs'].append(pos)
                xls.write_col(pos, "X")

    print rut_data
    xls.save(output_file_name) # Output File Name
    print ">>> Archivo de Salida: %s" % output_file_name
    print "*** Tarea Terminada! ***"

if __name__ == "__main__":
    main()
