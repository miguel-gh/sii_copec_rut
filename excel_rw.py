from openpyxl import load_workbook

wb = load_workbook('rut_julio_2016.xlsx')
print "Leyendo Sheet %s ..." % wb.get_sheet_names()

ws = wb.active

rut_val = ''
row = 2
elem = []

while rut_val is not None:
   rut_val = ws[("B%s" % row)].value
   if  ws[("B%s" % (row + 1))].value is None:
        break
   
   rut, dv = rut_val.split('-')
   elem.append({'rowid': row, 'rut': rut, 'dv': dv})
   row += 1

print "==="
print "%i Rut en total" % len(elem)

for r in elem:
    print r

